from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import os

from config_reader import config


def tt_photo_gen(i: int, price: int):
    """ generate an image fo tt post
    
    :param i: indes of photo
    :param price:
    :return: image ready to post
    """
    img = Image.new(mode="RGB", size=(1080, 1920))

    # photo of product
    fronteground_path = os.path.join(os.getcwd(), "data", "parser", f"item_photo{i}.png")
    fronteground = Image.open(fronteground_path)
    img_width, img_height = fronteground.size
    fronteground = fronteground.crop(((img_width - 1000) // 2,
                         (img_height - 1000) // 2,
                         (img_width + 1000) // 2,
                         (img_height + 1000) // 2))
    img.paste(fronteground, (40, 480))

    # adding text on img
    draw = ImageDraw.Draw(img)
    font_path = os.path.join(os.getcwd(), "generators", "Lora-Regular.ttf")
    font = ImageFont.truetype(font_path, 70)
    # main info
    draw.text((540, 250), "Артикулы в тг:", (255, 255, 255), font, align='center', anchor="mm")
    draw.text((540, 350), config.tg_link, (255, 255, 255), font, align='center', anchor="mm")
    # price
    draw.text((540, 1600), f'{price}', (255, 255, 255), font, align='center', anchor="mm")

    img_path = os.path.join(os.getcwd(), "data", "tt_post", f"res{i}.png")
    img.save(img_path)

