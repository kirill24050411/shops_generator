import random

from moviepy.editor import *

from config_reader import config


def video_gen(count: int):
    time = config.main_photo_duration
    src_clip = os.path.join(os.getcwd(), "data", "tt_post", "back_res.png")
    clip = ImageClip(src_clip).set_duration(time)

    arr = [clip]
    duration = config.photo_duration
    for i in range(0, count):
        time += duration
        src = os.path.join(os.getcwd(), "data", "tt_post", f"res{i}.png")
        clip = ImageClip(src).set_duration(duration)
        arr.append(clip)

    final_clip = concatenate_videoclips(arr, method="compose")

    if config.is_music_enabled:
        track_count = len(os.listdir(os.path.join(os.getcwd(), "data", "music")))
        num = random.randint(1, track_count)
        
        src_audio = os.path.join(os.getcwd(), "data", "music", f"track{num}.mp3")
        audio_clip = AudioFileClip(src_audio)
        final_clip = final_clip.set_audio(audio_clip)

    final_clip = final_clip.set_duration(time)

    final_clip_path = os.path.join(os.getcwd(), "data", "tt_post", "movie.mp4")
    final_clip.write_videofile(final_clip_path, fps=24)