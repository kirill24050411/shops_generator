import random

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import os


def first_photo_gen(txt: str):
    backs_count = len(os.listdir(os.path.join(os.getcwd(), "data", "backs")))
    i = random.randint(1, backs_count)
    back_path = os.path.join(os.getcwd(), "data", "backs", f'back{i}.jpg')
    back = Image.open(back_path)
    img = back.resize((1080, 1920))

    draw = ImageDraw.Draw(img)
    font_path = os.path.join(os.getcwd(), "generators", "Lora-Regular.ttf")
    font = ImageFont.truetype(font_path, 150)
    text = txt.replace('\p', ' ').replace('\\n', '\n')
    draw.text((540, 600), text, (255, 255, 255), font, align='center', anchor="mm")

    img_path = os.path.join(os.getcwd(), "data", "tt_post", 'back_res.png')
    img.save(img_path)
