from typing import List

from aiogram.filters import BaseFilter
from aiogram.types import Message

from config_reader import config


class IsAdmin(BaseFilter):
    async def __call__(self, message: Message) -> bool:
        return message.from_user.id in config.admin_ids
