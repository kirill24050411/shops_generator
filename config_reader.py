from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import SecretStr

from typing import List


class Settings(BaseSettings):
    bot_token: SecretStr
    admin_ids: List[int]
    tg_link: str
    main_photo_duration: int
    photo_duration: int
    is_music_enabled: bool
    model_config: SettingsConfigDict = SettingsConfigDict(
        env_file = ".env",
        env_file_encoding = "utf-8"
    )

config = Settings()